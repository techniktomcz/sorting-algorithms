package io.techniktom.sortingalgorithms.algorithms;

import io.techniktom.sortingalgorithms.SortingStatic;

import java.util.Arrays;
import java.util.function.IntFunction;
import java.util.stream.Collectors;

public class Counting extends SortingStatic<Integer> {

    public Counting(Integer[] unsortedArray) {
        super(unsortedArray);
    }

    @Override
    public Integer[] sortArray() {
        Integer highestNumber = Integer.MIN_VALUE;
        Integer lowestNumber = Integer.MAX_VALUE;

        for (Integer integer : array) {
            if (compare(integer, highestNumber) > 0) {
                highestNumber = integer;
            }
            if (compare(lowestNumber, integer) > 0) {
                lowestNumber = integer;
            }
        }

        int[] numberArray = new int[highestNumber - lowestNumber + 1];

        for (Integer integer : array) {
            numberArray[integer - lowestNumber]++;
        }

        for (int i = 1; i < numberArray.length; i++) {
            numberArray[i] += numberArray[i - 1];
        }

        Integer[] finalArray = new Integer[array.length];

        for (int i = 0; i < array.length; i++) {
            int position = array[i];
            int number = numberArray[position - lowestNumber] - 1;

            finalArray[number] = position;
            numberArray[position - lowestNumber]--;
        }

        return finalArray;
    }
}
