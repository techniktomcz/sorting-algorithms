package io.techniktom.sortingalgorithms;

import java.util.List;

public abstract class SortingDynamic<T> {

    protected List<T> array;

    public SortingDynamic(List<T> unsortedArray) {
        this.array = unsortedArray;
    }

    public abstract List<T> sortArray();

}
