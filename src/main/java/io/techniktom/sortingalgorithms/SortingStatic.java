package io.techniktom.sortingalgorithms;

import com.sun.org.apache.xpath.internal.operations.Bool;

import java.util.function.BiFunction;
import java.util.function.Predicate;

public abstract class SortingStatic<T extends Comparable<T>> {

    protected T[] array;
    protected T tmp;

    public SortingStatic(T[] unsortedArray) {
        this.array = unsortedArray;
    }

    public abstract T[] sortArray();

    protected Integer compare(T value1, T value2) {
        return value1.compareTo(value2);
    }
    protected BiFunction<T, T, Integer> compare = (T value1, T value2) -> value1.compareTo(value2);
    protected void swap(int index1, int index2) {
        tmp = array[index1];
        array[index1] = array[index2];
        array[index2] = tmp;

        tmp = null;
    }

    protected void swap(T[] array, int index1, int index2) {
        tmp = array[index1];
        array[index1] = array[index2];
        array[index2] = tmp;

        tmp = null;
    }
}
