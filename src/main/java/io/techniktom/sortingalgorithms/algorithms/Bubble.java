package io.techniktom.sortingalgorithms.algorithms;

import io.techniktom.sortingalgorithms.SortingStatic;

public class Bubble extends SortingStatic<Integer> {

    public Bubble(Integer[] unsortedArray) {
        super(unsortedArray);
    }

    @Override
    public Integer[] sortArray() {
        boolean changes;
        int lastIndex = array.length - 1;

        do {
            changes = false;

            for (int i = 0; i < lastIndex; i++) {
                if (compare(array[i], array[i + 1]) > 0) {
                    swap(i, i + 1);

                    changes = true;
                }
            }

            lastIndex--;
        } while (changes);

        return array;
    }
}
