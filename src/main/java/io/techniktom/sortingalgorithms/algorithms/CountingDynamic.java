package io.techniktom.sortingalgorithms.algorithms;

import io.techniktom.sortingalgorithms.SortingStatic;

import java.util.Arrays;
import java.util.stream.IntStream;

public class CountingDynamic extends SortingStatic<Integer> {

    public CountingDynamic(Integer[] unsortedArray) {
        super(unsortedArray);
    }

    @Override
    public Integer[] sortArray() {
        Integer high = Integer.MIN_VALUE;
        Integer low = Integer.MAX_VALUE;

        for (Integer integer : array) {
            if (compare(integer, high) > 0) {
                high = integer;
            }
            if (compare(low, integer) > 0) {
                low = integer;
            }
        }

        final int highestNumber = high;
        final int lowestNumber = low;

        int[] numberArray = new int[highestNumber - lowestNumber + 1];

        Arrays
            .stream(array)
            .parallel()
            .forEach(integer -> numberArray[integer - lowestNumber]++);

        for (int i = 1; i < numberArray.length; i++) {
            numberArray[i] += numberArray[i - 1];
        }

        Integer[] finalArray = new Integer[array.length];

        Arrays
            .stream(array)
            .parallel()
            .forEach(position -> {
                int number = numberArray[position - lowestNumber] - 1;

                finalArray[number] = position;
                numberArray[position - lowestNumber]--;
            });

        return finalArray;
    }
}
